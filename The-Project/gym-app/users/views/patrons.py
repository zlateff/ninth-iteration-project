from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import Count
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, ListView, UpdateView

from ..decorators import patron_required
from ..forms import PatronGoalsForm, PatronSignUpForm, DoRoutineForm
from ..models import Routine, Patron, WorkedRoutine, User


class PatronSignUpView(CreateView):
    model = User
    form_class = PatronSignUpForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'patron'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('patrons:routine_list')


@method_decorator([login_required, patron_required], name='dispatch')
class PatronGoalsView(UpdateView):
    model = Patron
    form_class = PatronGoalsForm
    template_name = 'users/patrons/goals_form.html'
    success_url = reverse_lazy('patrons:routine_list')

    def get_object(self):
        return self.request.user.patron

    def form_valid(self, form):
        messages.success(self.request, 'Goals updated with success!')
        return super().form_valid(form)


@method_decorator([login_required, patron_required], name='dispatch')
class RoutineListView(ListView):
    model = Routine
    ordering = ('name', )
    context_object_name = 'routines'
    template_name = 'users/patrons/routine_list.html'

    def get_queryset(self):
        patron = self.request.user.patron
        patron_goals = patron.goals.values_list('pk', flat=True)
        worked_routines = patron.routines.values_list('pk', flat=True)
        queryset = Routine.objects.filter(goal__in=patron_goals) \
            .exclude(pk__in=worked_routines) \
            .annotate(tasks_count=Count('tasks')) \
            .filter(tasks_count__gt=0)
        return queryset


@method_decorator([login_required, patron_required], name='dispatch')
class WorkedRoutinesListView(ListView):
    model = WorkedRoutine
    context_object_name = 'worked_routines'
    template_name = 'users/patrons/worked_routine_list.html'

    def get_queryset(self):
        queryset = self.request.user.patron.worked_routines \
            .select_related('routine', 'routine__goal') \
            .order_by('routine__name')
        return queryset


@login_required
@patron_required
def do_routine(request, pk):
    routine = get_object_or_404(Routine, pk=pk)
    patron = request.user.patron

    if patron.routines.filter(pk=pk).exists():
        return render(request, 'patrons/worked_routine.html')

    total_tasks = routine.tasks.count()
    incomplete_tasks = patron.get_uncompleted_tasks(routine)
    total_incomplete_tasks = incomplete_tasks.count()
    progress = 100 - round(((total_incomplete_tasks - 1) / total_tasks) * 100)
    task = incomplete_tasks.first()

    if request.method == 'POST':
        form = DoRoutineForm(task=task, data=request.POST)
        if form.is_valid():
            with transaction.atomic():
                patron_response = form.save(commit=False)
                patron_response.patron = patron
                patron_response.save()
                if patron.get_uncompleted_tasks(routine).exists():
                    return redirect('patrons:do_routine', pk)
                else:
                    complete_responses = patron.routine_responses.filter(response__task__routine=routine, response__is_complete=True).count()
                    score = round((complete_responses / total_tasks) * 100.0, 2)
                    WorkedRoutine.objects.create(patron=patron, routine=routine, score=score)
                    if score < 50.0:
                        messages.warning(request, 'Try wroking harder the next time! Your completion score for the routine %s was %s.' % (routine.name, score))
                    else:
                        messages.success(request, 'Awesome work! You completed the routine %s with success! You completed %s percent of the work!' % (routine.name, score))
                    return redirect('patrons:routine_list')
    else:
        form = DoRoutineForm(task=task)

    return render(request, 'users/patrons/do_routine_form.html', {
        'routine': routine,
        'task': task,
        'form': form,
        'progress': progress
    })
