from django.contrib import admin
from django.urls import include, path

from users.views import users, patrons, trainers

urlpatterns = [
    path('', include('users.urls')),

    # Django Admin
    path('admin/', admin.site.urls),

    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/signup/', users.SignUpView.as_view(), name='signup'),
    path('accounts/signup/patrons/', patrons.PatronSignUpView.as_view(), name='patron_signup'),
    path('accounts/signup/trainers/', trainers.TrainerSignUpView.as_view(), name='trainer_signup'),
]
