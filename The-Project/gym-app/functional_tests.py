from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import unittest
import names


port = '1234'
print("Make sure you have set the port variable if the page isn't opening.")


class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_can_register_trainer(self):
        user_name = names.get_first_name()

        # Biff has heard about a new online gym app for trainers:
        # He goes to the webspyite to check out its home page.
        self.browser.get('http://localhost:' + port)

        # He notices the title page and header mention creating a trainer account.
        self.assertIn('The Gym', self.browser.title)
        header_two_text = self.browser.find_element_by_tag_name('h2').text
        self.assertIn('Welcome to The Gym', header_two_text)
        time.sleep(1)

        # He decides to register for a trainer account, so he click the "trainer account" link.
        self.browser.find_element_by_link_text('trainer account').click()

        # He sees that he can sign up as a trainer from this page.
        self.assertIn('Sign up as trainer', self.browser.title)

        # He enters a username into the username field
        username_inputbox = self.browser.find_element_by_id("id_username")

        # He types in a username BuffBiff
        username_inputbox.send_keys(user_name)
        time.sleep(1)

        # He enters a password
        password1_inputbox = self.browser.find_element_by_id("id_password1")
        password2_inputbox = self.browser.find_element_by_id("id_password2")

        # He types in a secure password
        password1_inputbox.send_keys("this is a password")
        password2_inputbox.send_keys("this is a password")
        time.sleep(1)
        password2_inputbox.send_keys(Keys.ENTER)
        time.sleep(1)

        # Finished with the site, he logs out.
        self.browser.find_element_by_link_text('Log out').click()

    def test_can_register_patron(self):
        user_name = names.get_first_name()

        # Cletus has heard about a new online gym app for patrons.
        # He goes to the website to check out its home page.
        self.browser.get('http://localhost:' + port)
        time.sleep(1)

        # He notices that the title page and header mention creating a patron account.
        self.assertIn('The Gym', self.browser.title)
        header_two_text = self.browser.find_element_by_tag_name('h2').text
        self.assertIn('Welcome to The Gym', header_two_text)
        time.sleep(1)

        # Cletus is a patron, so he decides to click the 'patron account" link.
        self.browser.find_element_by_link_text('patron account').click()
        time.sleep(1)

        # He enters a username into the username field
        username_inputbox = self.browser.find_element_by_id("id_username")

        # He types in a username BuffBiff
        username_inputbox.send_keys(user_name)
        time.sleep(1)

        # He selects the "Get Stronger" Goal.
        # self.browser.find_element_by_xpath(".//*[contains(text(), 'id_goals_1')]").click()
        self.browser.find_element_by_id("id_goals_1").click()

        # He enters a password
        password1_inputbox = self.browser.find_element_by_id("id_password1")
        password2_inputbox = self.browser.find_element_by_id("id_password2")

        # He types in a secure password
        password1_inputbox.send_keys("this is a password")
        password2_inputbox.send_keys("this is a password")
        time.sleep(1)
        password2_inputbox.send_keys(Keys.ENTER)
        time.sleep(1)

        # Finished with the site, he logs out.
        self.browser.find_element_by_link_text('Log out').click()
        time.sleep(1)


if __name__ == '__main__':
    unittest.main()
