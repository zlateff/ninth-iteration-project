# Dev Environment Setup

Requirements and instructions for setting up a local development environment. 

## Required Software

The following is the software you are going to need on your local machine.

##### 1. Oracle VM VirtualBox
Download [VirtualBox](https://www.virtualbox.org/wiki/Downloads) for the OS you are running and install it. 
The installation is straightforward, just go with the defaults.

##### 2. Vagrant
Download [Vagrant](https://www.vagrantup.com/downloads.html) for the OS you are running and install it. 
You might have to restart after installing Vagrant.

##### 3. Git
You need to have [Git](https://git-scm.com/downloads) installed on your machine. 
Follow the instructions for your operating system.

## Configuration
Once you have the required software installed on your machine, follow these steps:

- Download the repository, go to https://bitbucket.org/ninthiteration/vagrant-setup/downloads/

- Unzpip the repo files to a directory where you want your project, you only need the files. 
    The Unzip creates nested directories, feel free to move the files where you want them
    I'll assume they are in `~/NinthIteration`

- Open the Terminal or start Git Bash if you are running Windows.

- Navigate to the directory where you put your files. I'll assume `cd /NinthIteration`

- Download the vagrant box `$ vagrant box add ubuntu/trusty64` Be patient, it will take a while.

- Install this plugin `$ vagrant plugin install vagrant-vbguest`

- Type the `vagrant up` command. 

The first time you run `vagrant up`, it will take little longer for all the packages to be istalled.

Please be patient, you will have to wait this long only the first time.

Once the setup script is done, you are going to have a guest VM running.

If you open the Oracle VM VirtualBox application, you are going to see your instance there as well.

You don't need to have VirtualBox open, Vagrant takes care of everything. 

## Create the project

- Do `$ vagrant ssh` 

- Follow steps 1 to 6 that are shown in the welcome message to create the project.


    - (1) `$ cd /vagrant/projects`

    - (2) `$ virtualenv eb-virt --always-copy`

    - (3) `$ envactivate`

    - (4) `$ pip3 install django`

    - (5) `$ $ pip3 install djangorestframework`
    
    - (6) On your machine, `$ cd NinthIteration/projects` followed by `$ git clone https://zlateff@bitbucket.org/ninthiteration/gymapp.git`
    
    - (7) $ runserver
    
To confirm that you are set, open http://127.0.0.1:8000/ on your machine's web browser.

You should see the sample Django page.


## Starting and Stoping the VM after you have the project

#### To Start
- Every time, before `$ vagrant up` , you must navigate to the project directory first `cd ~/NinthIteration`

- After the VM is up, `$ vagrant ssh` to SSH inside the VM.

- Next, you need to activate the python virtual environment, use `$ envactivate`.

- To start the server, use `$ runserver`.

To confirm that you are set, open http://127.0.0.1:8000/ on your machine's web browser.

You should see the sample Django page.

#### To Stop
When you are done working, follow these steps:

- `Ctr-C` to stop the server

- `$ deactivate` the python vrtual environment

- `$ exit` the VM

- `$ vagrant halt` to shut down the VM


## Project Directory and Git setup
`~/NinthIteration` is a shared directory between your machine and the guest VM.

This directory corresponds to `/vagrant` inside the VM. 

The project directory is `~/NinthIteration/projects/gymapp` 

To setup git, do this on your own(the host) machine:

- `$ cd /NinthIteration/projects/gymapp`

- `$ git init`

- `$ git remote add origin https://zlateff@bitbucket.org/ninthiteration/gymapp.git`

## Additional References
For information on Vagrant you can go to this [Getting Started Page](https://www.vagrantup.com/intro/getting-started/). 

Please follow the instructions listed in this document and don't use the example setup on the Getting Started Page. 

You'll end up with a different VM if you do that. 

For help with git, check out this [Tutorial](https://www.atlassian.com/git/tutorials/setting-up-a-repository).
