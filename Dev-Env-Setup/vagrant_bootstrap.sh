#!/bin/bash

#Refresh the apt package index
apt-get -qqy update

#Install the pip3 Python package manager
apt-get install -qqy python3-pip

#Install virtualenv
pip3 install virtualenv

#Setup aliases and welcome message
echo 'Setting up message, and some aliases...'
cp /vagrant/vagrant_up_msg.txt /etc/motd
printf "alias menu='cat /etc/motd'\n" >> ~vagrant/.bashrc
printf "alias envactivate='source /vagrant/projects/eb-virt/bin/activate'\n" >> ~vagrant/.bashrc
printf "alias runserver='cd /vagrant/projects/gymapp; python manage.py runserver 0.0.0.0:8000'\n" >> ~vagrant/.bashrc

# Complete
echo ""
echo "Vagrant install complete."
echo "Now try logging in:"
echo "    $ vagrant ssh"
