from django.urls import include, path

from .views import users, patrons, trainers

urlpatterns = [
    path('', users.home, name='home'),

    path('about/', users.about, name='about'),

    path('patrons/', include(([
        path('', patrons.RoutineListView.as_view(), name='routine_list'),
        path('goals/', patrons.PatronGoalsView.as_view(), name='patron_goals'),
        path('worked/', patrons.WorkedRoutinesListView.as_view(), name='worked_routine_list'),
        path('routine/<int:pk>/', patrons.do_routine, name='do_routine'),
    ], 'users'), namespace='patrons')),

    path('trainers/', include(([
        path('', trainers.RoutineListView.as_view(), name='routine_change_list'),
        path('routine/add/', trainers.RoutineCreateView.as_view(), name='routine_add'),
        path('routine/<int:pk>/', trainers.RoutineUpdateView.as_view(), name='routine_change'),
        path('routine/<int:pk>/delete/', trainers.RoutineDeleteView.as_view(), name='routine_delete'),
        path('routine/<int:pk>/results/', trainers.RoutineResultsView.as_view(), name='routine_results'),
        path('routine/<int:pk>/task/add/', trainers.task_add, name='task_add'),
        path('routine/<int:routine_pk>/task/<int:task_pk>/', trainers.task_change, name='task_change'),
        path('routine/<int:routine_pk>/task/<int:task_pk>/delete/', trainers.TaskDeleteView.as_view(), name='task_delete'),
    ], 'users'), namespace='trainers')),
]
