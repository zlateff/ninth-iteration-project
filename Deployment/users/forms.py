from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction
from django.forms.utils import ValidationError

from users.models import (Response, Task, Patron, PatronResponse,
                              Goal, User)


class TrainerSignUpForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User

    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_trainer = True
        if commit:
            user.save()
        return user


class PatronSignUpForm(UserCreationForm):
    goals = forms.ModelMultipleChoiceField(
        queryset=Goal.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )

    class Meta(UserCreationForm.Meta):
        model = User

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_patron = True
        user.save()
        patron = Patron.objects.create(user=user)
        patron.goals.add(*self.cleaned_data.get('goals'))
        return user


class PatronGoalsForm(forms.ModelForm):
    class Meta:
        model = Patron
        fields = ('goals', )
        widgets = {
            'goals': forms.CheckboxSelectMultiple
        }


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ('text', 'video', )


class BaseResponseInlineFormSet(forms.BaseInlineFormSet):
    def clean(self):
        super().clean()

        has_one_mandatory_response = False
        for form in self.forms:
            if not form.cleaned_data.get('DELETE', False):
                if form.cleaned_data.get('is_complete', False):
                    has_one_mandatory_response = True
                    break
        if not has_one_mandatory_response:
            raise ValidationError('Mark at least one response as mandatory.', code='no_mandatory_response')


class DoRoutineForm(forms.ModelForm):
    response = forms.ModelChoiceField(
        queryset=Response.objects.none(),
        widget=forms.RadioSelect(),
        required=True,
        empty_label=None)

    class Meta:
        model = PatronResponse
        fields = ('response', )

    def __init__(self, *args, **kwargs):
        task = kwargs.pop('task')
        super().__init__(*args, **kwargs)
        self.fields['response'].queryset = task.responses.order_by('text')
