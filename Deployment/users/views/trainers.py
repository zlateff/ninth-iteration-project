from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import Avg, Count
from django.forms import inlineformset_factory
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import (CreateView, DeleteView, DetailView, ListView,
                                  UpdateView)

from ..decorators import trainer_required
from ..forms import BaseResponseInlineFormSet, TaskForm, TrainerSignUpForm
from ..models import Response, Task, Routine, User


class TrainerSignUpView(CreateView):
    model = User
    form_class = TrainerSignUpForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'trainer'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('trainers:routine_change_list')


@method_decorator([login_required, trainer_required], name='dispatch')
class RoutineListView(ListView):
    model = Routine
    ordering = ('name', )
    context_object_name = 'routines'
    template_name = 'users/trainers/routine_change_list.html'

    def get_queryset(self):
        queryset = self.request.user.routines \
            .select_related('goal') \
            .annotate(tasks_count=Count('tasks', distinct=True)) \
            .annotate(worked_count=Count('worked_routines', distinct=True))
        return queryset


@method_decorator([login_required, trainer_required], name='dispatch')
class RoutineCreateView(CreateView):
    model = Routine
    fields = ('name', 'goal', )
    template_name = 'users/trainers/routine_add_form.html'

    def form_valid(self, form):
        routine = form.save(commit=False)
        routine.owner = self.request.user
        routine.save()
        messages.success(self.request, 'The routine was created with success! Go ahead and add some tasks now.')
        return redirect('trainers:routine_change', routine.pk)


@method_decorator([login_required, trainer_required], name='dispatch')
class RoutineUpdateView(UpdateView):
    model = Routine
    fields = ('name', 'goal', )
    context_object_name = 'routine'
    template_name = 'users/trainers/routine_change_form.html'

    def get_context_data(self, **kwargs):
        kwargs['tasks'] = self.get_object().tasks.annotate(response_count=Count('responses'))
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        '''
        This method is an implicit object-level permission management
        This view will only match the ids of existing routines that belongs
        to the logged in user.
        '''
        return self.request.user.routines.all()

    def get_success_url(self):
        return reverse('trainers:routine_change', kwargs={'pk': self.object.pk})


@method_decorator([login_required, trainer_required], name='dispatch')
class RoutineDeleteView(DeleteView):
    model = Routine
    context_object_name = 'routine'
    template_name = 'users/trainers/routine_delete_confirm.html'
    success_url = reverse_lazy('trainers:routine_change_list')

    def delete(self, request, *args, **kwargs):
        routine = self.get_object()
        messages.success(request, 'The routine %s was deleted with success!' % routine.name)
        return super().delete(request, *args, **kwargs)

    def get_queryset(self):
        return self.request.user.routines.all()


@method_decorator([login_required, trainer_required], name='dispatch')
class RoutineResultsView(DetailView):
    model = Routine
    context_object_name = 'routine'
    template_name = 'users/trainers/routine_results.html'

    def get_context_data(self, **kwargs):
        routine = self.get_object()
        worked_routines = routine.worked_routines.select_related('patron__user').order_by('-date')
        total_worked_routines = worked_routines.count()
        routine_score = routine.worked_routines.aggregate(average_score=Avg('score'))
        extra_context = {
            'worked_routines': worked_routines,
            'total_worked_routines': total_worked_routines,
            'routine_score': routine_score
        }
        kwargs.update(extra_context)
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        return self.request.user.routines.all()


@login_required
@trainer_required
def task_add(request, pk):
    routine = get_object_or_404(Routine, pk=pk, owner=request.user)

    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.routine = routine
            task.save()
            messages.success(request, 'You may now add response options to the task.')
            return redirect('trainers:task_change', routine.pk, task.pk)
    else:
        form = TaskForm()

    return render(request, 'users/trainers/task_add_form.html', {'routine': routine, 'form': form})


@login_required
@trainer_required
def task_change(request, routine_pk, task_pk):
    routine = get_object_or_404(Routine, pk=routine_pk, owner=request.user)
    task = get_object_or_404(Task, pk=task_pk, routine=routine)

    ResponseFormSet = inlineformset_factory(
        Task,  
        Response,  
        formset=BaseResponseInlineFormSet,
        fields=('text', 'is_complete'),
        min_num=2,
        validate_min=True,
        max_num=5,
        validate_max=True
    )

    if request.method == 'POST':
        form = TaskForm(request.POST, instance=task)
        formset = ResponseFormSet(request.POST, instance=task)
        if form.is_valid() and formset.is_valid():
            with transaction.atomic():
                form.save()
                formset.save()
            messages.success(request, 'Task and response options saved with success!')
            return redirect('trainers:routine_change', routine.pk)
    else:
        form = TaskForm(instance=task)
        formset = ResponseFormSet(instance=task)

    return render(request, 'users/trainers/task_change_form.html', {
        'routine': routine,
        'task': task,
        'form': form,
        'formset': formset
    })


@method_decorator([login_required, trainer_required], name='dispatch')
class TaskDeleteView(DeleteView):
    model = Task
    context_object_name = 'task'
    template_name = 'users/trainers/task_delete_confirm.html'
    pk_url_kwarg = 'task_pk'

    def get_context_data(self, **kwargs):
        task = self.get_object()
        kwargs['routine'] = task.routine
        return super().get_context_data(**kwargs)

    def delete(self, request, *args, **kwargs):
        task = self.get_object()
        messages.success(request, 'The task %s was deleted with success!' % task.text)
        return super().delete(request, *args, **kwargs)

    def get_queryset(self):
        return Task.objects.filter(routine__owner=self.request.user)

    def get_success_url(self):
        task = self.get_object()
        return reverse('trainers:routine_change', kwargs={'pk': task.routine_id})
