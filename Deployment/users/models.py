from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.html import escape, mark_safe


class User(AbstractUser):
    is_patron = models.BooleanField(default=False)
    is_trainer = models.BooleanField(default=False)


class Goal(models.Model):
    name = models.CharField(max_length=30)
    color = models.CharField(max_length=7, default='#007bff')

    def __str__(self):
        return self.name

    def get_html_badge(self):
        name = escape(self.name)
        color = escape(self.color)
        html = '<span class="badge badge-primary" style="background-color: %s">%s</span>' % (color, name)
        return mark_safe(html)


class Routine(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='routines')
    name = models.CharField(max_length=255)
    goal = models.ForeignKey(Goal, on_delete=models.CASCADE, related_name='routines')

    def __str__(self):
        return self.name


class Task(models.Model):
    routine = models.ForeignKey(Routine, on_delete=models.CASCADE, related_name='tasks')
    text = models.CharField('Task Name', max_length=255)
    video = models.CharField('Video embed URL', max_length=255)

    def __str__(self):
        return self.text


class Response(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='responses')
    text = models.CharField('Response', max_length=255)
    is_complete = models.BooleanField('Task Completed', default=False)

    def __str__(self):
        return self.text


class Patron(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    routines = models.ManyToManyField(Routine, through='WorkedRoutine')
    goals = models.ManyToManyField(Goal, related_name='patron_goals')

    def get_uncompleted_tasks(self, routine):
        completed_tasks = self.routine_responses \
            .filter(response__task__routine=routine) \
            .values_list('response__task__pk', flat=True)
        tasks = routine.tasks.exclude(pk__in=completed_tasks).order_by('text')
        return tasks

    def __str__(self):
        return self.user.username

class Trainer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    experience = models.CharField('Experience', max_length=255)
    bio = models.CharField('Biography', max_length=255)

    def __str__(self):
        return self.user.username


class WorkedRoutine(models.Model):
    patron = models.ForeignKey(Patron, on_delete=models.CASCADE, related_name='worked_routines')
    routine = models.ForeignKey(Routine, on_delete=models.CASCADE, related_name='worked_routines')
    score = models.FloatField()
    date = models.DateTimeField(auto_now_add=True)


class PatronResponse(models.Model):
    patron = models.ForeignKey(Patron, on_delete=models.CASCADE, related_name='routine_responses')
    response = models.ForeignKey(Response, on_delete=models.CASCADE, related_name='+')
